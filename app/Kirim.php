<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kirim extends Model
{
    protected $fillable = ['nama_pengirim'];
}
