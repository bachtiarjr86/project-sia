

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Optime Logistics &amp; Transportation Templatei">
  <link href="assets/images/favicon/favicon.png" rel="icon">
  <title>Cek Ongkir - JNJ Express</title>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:400,500,700%7cPoppins:400,600,700&display=swap">
  <link rel="stylesheet" href="{{asset('assets/css/libraries.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
  <div class="wrapper">
    <!-- =========================
        Header
    =========================== -->    
    <header id="header" class="header header-transparent" style="background-color: #f2a51a;">
      <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html">
            <img src="{{asset('assets/images/logo/logo-light.png')}}" class="logo-light" alt="logo">
            <img src="{{asset('assets/images/logo/logo-dark.png')}}" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>         
          <div class="collapse navbar-collapse" id="mainNavigation">         
            <ul class="navbar-nav ml-auto">                       
              <li class="nav__item with-dropdown">
                <a href="/" class="dropdown-toggle nav__item-link">Home</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/prosestracking" class="dropdown-toggle nav__item-link">Proses Tracking</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="services.html" class="dropdown-toggle nav__item-link">Status Pengiriman</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/cekongkir" class="dropdown-toggle nav__item-link active">Cek Ongkir</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
                <li class="nav__item with-dropdown">
                    <a href="#" class="dropdown-toggle nav__item-link" style="color: #10375c;" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav__item"> 
                            <a href="{{ route('logout') }}" class="nav__item-link"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li><!-- /.nav-item -->
                  <!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->                
              </li><!-- /.nav-item -->                                      
            </ul><!-- /.navbar-nav -->
          </div><!-- /.navbar-collapse -->          
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">Cek Ongkos Kirim</h4>
                    </div>
                    <div class="panel-body">
                            <div>
                                <?php
                                //Get Data Kabupaten
                                $curl = curl_init();
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "GET",
                                    CURLOPT_HTTPHEADER => array(
                                        "key:697aec56c7cbf87b80c1296def7aa008"
                                    ),
                                ));

                                $response = curl_exec($curl);
                                $err = curl_error($curl);

                                curl_close($curl);
                                echo "
                                <div class= \"form-group\">
                                <label for=\"asal\">Kota/Kabupaten Asal </label>
                                <select class=\"form-control\" name='asal' id='asal'>";
                                echo "<option>Pilih Kota Asal</option>";
                                $data = json_decode($response, true);
                                for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
                                    echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
                                }
                                echo "</select>
                                </div>";
                                //Get Data Kabupaten
                                //-----------------------------------------------------------------------------

                                //Get Data Provinsi
                                $curl = curl_init();

                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "GET",
                                    CURLOPT_HTTPHEADER => array(
                                    "key:697aec56c7cbf87b80c1296def7aa008"
                                    ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);

                                    echo "
                                    <div class= \"form-group\">
                                        <label for=\"provinsi\">Provinsi Tujuan </label>
                                        <select class=\"form-control\" name='provinsi' id='provinsi'>";
                                            echo "<option>Pilih Provinsi Tujuan</option>";
                                            $data = json_decode($response, true);
                                            for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
                                                echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
                                            }
                                            echo "</select>
                                        </div>";
                                        //Get Data Provinsi
                                        ?>

                                        <div class="form-group">
                                            <label for="kabupaten">Kota/Kabupaten Tujuan</label><br>
                                            <select class="form-control" id="kabupaten" name="kabupaten"></select>
                                        </div>
                                        <div class="form-group">
                                            <label for="kurir">Kurir</label><br>
                                            <select class="form-control" id="kurir" name="kurir">
                                                <option value="jne">JNE</option>
                                                <option value="tiki">TIKI</option>
                                                <option value="pos">POS INDONESIA</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="berat">Berat (gram)</label><br>
                                            <input class="form-control" id="berat" type="text" name="berat" value="500" />
                                        </div>
                                        <button class="btn" id="cek" type="submit" name="button" style="background-color: #10375c; color: white;">Cek Ongkir</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h5 class="panel-title">Hasil</h5>
                            </div>
                            <div class="panel-body">
                                <ol>
                                    <div id="ongkir"></div>
                                </div>
                            </ol>
                        </div>
                    </div>
        </div>
        </div>
    </div>         

    <div class="module__search-container">
      <i class="fa fa-times close-search"></i>
      <form class="module__search-form">
        <input type="text" class="search__input" placeholder="Type Words Then Enter">
        <button class="module__search-btn"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- /.module-search-container -->

    <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
  <script type="text/javascript">
        $(document).ready(function(){
            $('#provinsi').change(function(){

                //Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax
                var prov = $('#provinsi').val();

                $.ajax({
                    type : 'GET',
                    url : '{{asset('assets/cek_kabupaten.php')}}',
                    data :  'prov_id=' + prov,
                        success: function (data) {

                        //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
                        $("#kabupaten").html(data);
                    }
                });
            });

            $("#cek").click(function(){
                //Mengambil value dari option select provinsi asal, kabupaten, kurir, berat kemudian parameternya dikirim menggunakan ajax
                var asal = $('#asal').val();
                var kab = $('#kabupaten').val();
                var kurir = $('#kurir').val();
                var berat = $('#berat').val();

                $.ajax({
                    type : 'POST',
                    url : '{{asset('assets/cek_ongkir.php')}}',
                    data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat},
                        success: function (data) {

                        //jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
                        $("#ongkir").html(data);
                    }
                });
            });
        });
    </script>
</body>

</html>
