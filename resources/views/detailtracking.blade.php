

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Optime Logistics &amp; Transportation Templatei">
  <link href="assets/images/favicon/favicon.png" rel="icon">
  <title>Proses Tracking - JNJ Express</title>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:400,500,700%7cPoppins:400,600,700&display=swap">
  <link rel="stylesheet" href="{{asset('assets/css/libraries.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
  <div class="wrapper">
    <!-- =========================
        Header
    =========================== -->    
    <header id="header" class="header header-transparent" style="background-color: #f2a51a;">
      <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html">
            <img src="{{asset('assets/images/logo/logo-light.png')}}" class="logo-light" alt="logo">
            <img src="{{asset('assets/images/logo/logo-dark.png')}}" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>         
          <div class="collapse navbar-collapse" id="mainNavigation">         
            <ul class="navbar-nav ml-auto">                       
              <li class="nav__item with-dropdown">
                <a href="/" class="dropdown-toggle nav__item-link">Home</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="#" class="dropdown-toggle nav__item-link active">Proses Tracking</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="services.html" class="dropdown-toggle nav__item-link">Status Pengiriman</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/cekongkir" class="dropdown-toggle nav__item-link">Cek Ongkir</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
                <li class="nav__item with-dropdown">
                    <a href="#" class="dropdown-toggle nav__item-link" style="color: #10375c;" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav__item"> 
                            <a href="{{ route('logout') }}" class="nav__item-link"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li><!-- /.nav-item -->
                  <!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->                
              </li><!-- /.nav-item -->                                      
            </ul><!-- /.navbar-nav -->
          </div><!-- /.navbar-collapse -->          
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <br>
    @foreach ($kirims as $kirim)
        {{ $kirim->nama_pengirim }} 
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <th scope="row">3</th>
                <td colspan="2">Larry the Bird</td>
                <td>@twitter</td>
                </tr>
            </tbody>
        </table>
    @endforeach       

    <div class="module__search-container">
      <i class="fa fa-times close-search"></i>
      <form class="module__search-form">
        <input type="text" class="search__input" placeholder="Type Words Then Enter">
        <button class="module__search-btn"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- /.module-search-container -->

    <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>
