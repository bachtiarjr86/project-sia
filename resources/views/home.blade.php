<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Optime Logistics &amp; Transportation Templatei">
  <link href="assets/images/favicon/favicon.png" rel="icon">
  <title>Home - JNJ Express</title>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:400,500,700%7cPoppins:400,600,700&display=swap">
  <link rel="stylesheet" href="{{asset('assets/css/libraries.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
</head>

<body>
  <div class="wrapper">
    <!-- =========================
        Header
    =========================== -->    
    <header id="header" class="header header-transparent">
      <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html">
            <img src="{{asset('assets/images/logo/logo-light.png')}}" class="logo-light" alt="logo">
            <img src="{{asset('assets/images/logo/logo-dark.png')}}" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>         
          <div class="collapse navbar-collapse" id="mainNavigation">         
            <ul class="navbar-nav ml-auto">                       
              <li class="nav__item with-dropdown">
                <a href="index.html" class="dropdown-toggle nav__item-link active">Home</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/prosestracking" class="dropdown-toggle nav__item-link">Proses Tracking</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="services.html" class="dropdown-toggle nav__item-link">Status Pengiriman</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/cekongkir" class="dropdown-toggle nav__item-link">Cek Ongkir</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
                <li class="nav__item with-dropdown">
                    <a href="#" class="dropdown-toggle nav__item-link" style="color: #ea6227;" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav__item"> 
                            <a href="{{ route('logout') }}" class="nav__item-link"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li><!-- /.nav-item -->
                  <!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->                
              </li><!-- /.nav-item -->                                      
            </ul><!-- /.navbar-nav -->
          </div><!-- /.navbar-collapse -->          
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <!-- ============================
        Slider
    ============================== -->
    <section id="slider1" class="slider slider-1">
      <div class="owl-carousel thumbs-carousel carousel-arrows" data-slider-id="slider1" data-dots="false"
        data-autoplay="true" data-nav="true" data-transition="fade" data-animate-out="fadeOut" data-animate-in="fadeIn">
        <div class="slide-item align-v-h bg-overlay">
          <div class="bg-img"><img src="{{asset('assets/images/sliders/1.jpg')}}" alt="slide img"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="slide__content">
                  <h2 class="slide__title">Solusi Cepat, Bersertifikat & Fleksibel.</h2>
                  <p class="slide__desc">Melalui solusi rantai pasokan terintegrasi, drive kami kompetitif berkelanjutan keuntungan untuk beberapa perusahaan terbesar di seluruh dunia.</p>
                  <a href="#" class="btn btn__primary btn__hover2 mr-30">Our Services</a>
                  <a href="#" class="btn btn__white">About Us</a>
                </div><!-- /.slide-content -->
              </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
          </div><!-- /.container -->
        </div><!-- /.slide-item -->
        <div class="slide-item align-v-h bg-overlay">
          <div class="bg-img"><img src="{{asset('assets/images/sliders/4.jpg')}}" alt="slide img"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="slide__content">
                  <h2 class="slide__title">Affordable Price & Great Solutions.</h2>
                  <p class="slide__desc">Through integrated supply chain solutions, our drives sustainable competitive
                    advantages to some of the largest companies allover the world.</p>
                  <a href="#" class="btn btn__white mr-30">About Us</a>
                  <a href="#" class="btn btn__primary btn__hover2">Our Services</a>
                </div><!-- /.slide-content -->
              </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
          </div><!-- /.container -->
        </div><!-- /.slide-item -->
        <div class="slide-item align-v-h bg-overlay">
          <div class="bg-img"><img src="{{asset('assets/images/sliders/5.jpg')}}" alt="slide img"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="slide__content">
                  <h2 class="slide__title">Solutions Matchs Your Needs. </h2>
                  <p class="slide__desc">Through integrated supply chain solutions, our drives sustainable competitive
                    advantages to some of the largest companies allover the world.</p>
                  <a href="#" class="btn btn__primary btn__hover2 mr-30">Our Services</a>
                  <a href="#" class="btn btn__white">About Us</a>
                </div><!-- /.slide-content -->
              </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
          </div><!-- /.container -->
        </div><!-- /.slide-item -->
        <div class="slide-item align-v-h bg-overlay">
          <div class="bg-img"><img src="{{asset('assets/images/sliders/3.jpg')}}" alt="slide img"></div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="slide__content">
                  <h2 class="slide__title">Affordable Price, Certified experts &</h2>
                  <p class="slide__desc">Through integrated supply chain solutions, our drives sustainable competitive
                    advantages to some of the largest companies allover the world.</p>
                  <a href="#" class="btn btn__white mr-30">Get Started</a>
                  <a href="#" class="btn btn__primary btn__hover2">Our Services</a>
                </div><!-- /.slide-content -->
              </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
          </div><!-- /.container -->
        </div><!-- /.slide-item -->
      </div><!-- /.carousel -->
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 d-none d-lg-block">
            <div class="owl-thumbs thumbs-dots" data-slider-id="slider1">
              <button class="owl-thumb-item">
                <i class="icon-forklift-1"></i>
                <span>Warehousing <br> Services</span>
              </button>
              <button class="owl-thumb-item">
                <i class="icon-air-freight"></i>
                <span>Air Freight<br> Services</span>
              </button>
              <button class="owl-thumb-item">
                <i class="icon-cargo-ship"></i>
                <span>Ocean Freight<br> Services</span>
              </button>
              <button class="owl-thumb-item">
                <i class="icon-truck"></i>
                <span>Road Freight<br> Services</span>
              </button>
            </div><!-- /.owl-thumbs -->
          </div><!-- /.col-lg-12 -->
          <div class="col-sm-12 col-md-12 col-lg-12 d-none d-lg-block">
            <a class="btn btn__video btn__video-lg popup-video" href="https://www.youtube.com/watch?v=nrJtHemSPW4">
              <div class="video__player">
                <span class="video__player-animation"></span>
                <span class="video__player-animation video__player-animation-2"></span>
                <i class="fa fa-play"></i>
              </div>
              <span class="video__btn-title">Watch Our Presentation!</span>
            </a><!-- /.btn__video-->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.slider -->

    <!-- ========================
        Services
    =========================== -->
    
    <!-- =========================== 
      fancybox Carousel
    ============================= -->
    <section id="fancyboxCarousel"
      class="fancybox-layout4 fancybox-carousel bg-overlay bg-overlay-gradient-secondary-2">
      <div class="bg-img"><img src="{{asset('assets/images/backgrounds/5.jpg')}}" alt="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
            <div class="heading text-center mb-40">
              <span class="heading__subtitle">Our Features</span>
              <h2 class="heading__title color-white">Why Choose Us!</h2>
              <p class="heading__desc">We continue to pursue that same vision in today's complex, uncertain world,
                working every day to earn our customers’ trust!</p>
            </div><!-- /.heading -->
          </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="carousel owl-carousel carousel-arrows" data-slide="4" data-slide-md="2" data-slide-sm="1"
              data-autoplay="true" data-nav="true" data-dots="false" data-space="20" data-loop="true" data-speed="800">
              <!-- fancybox item #1 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-wallet"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Transparent Pricing</h4>
                  <p class="fancybox__desc">International supply chains involves challenging regulations.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #2 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-search"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Real-Time Tracking</h4>
                  <p class="fancybox__desc">Ensure customers’ supply chains are fully compliant by practices.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #3 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-trolley"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Warehouse Storage</h4>
                  <p class="fancybox__desc">Depending on your product, we provide warehouse activities.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #4 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-package-6"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Security For Cargo</h4>
                  <p class="fancybox__desc">High security requirements and are certified to local standards.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #5 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-payment-method"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Easy Payment Methods</h4>
                  <p class="fancybox__desc">You can make use the easy payment options, listed below.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #6 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-delivery-4"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Fast & Efficient Delivery</h4>
                  <p class="fancybox__desc">We enhance our operations by relieving you of the worries.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
              <!-- fancybox item #7 -->
              <div class="fancybox-item">
                <div class="fancybox__icon">
                  <i class="icon-wishlist"></i>
                </div><!-- /.fancybox-icon -->
                <div class="fancybox__content">
                  <h4 class="fancybox__title">Personalised solutions</h4>
                  <p class="fancybox__desc">We continue to pursue that same vision in today's complex solutions.</p>
                </div><!-- /.fancybox-content -->
              </div><!-- /.fancybox-item -->
            </div><!-- /.carousel -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <p class="text__link mb-0">Working every day to earn our customers’ trust. <a href="#"
                class="color-theme">Get Started!</a>
            </p>
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.fancybox Carousel -->

    <!-- ========================
        Request Quote Tabs
    =========================== -->
    <section id="requestQuoteTabs" class="request-quote request-quote-tabs p-0">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="request__form">
              <nav class="nav nav-tabs">
                <a class="nav__link active" data-toggle="tab" href="#quote">Request A Quote</a>
                <a class="nav__link" data-toggle="tab" href="#track">Track & Trace</a>
              </nav>
              <div class="tab-content">
                <div class="tab-pane fade show active" id="quote">
                  <div class="request-quote-panel">
                    <div class="request__form-body">
                      <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <h6>Personal Data</h6>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name">
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email">
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone">
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <h6 class="mt-5">Shipment Data</h6>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group form-group-select">
                            <select class="form-control">
                              <option>Freight Type</option>
                              <option>Freight Type 1</option>
                              <option>Freight Type 2</option>
                            </select>
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group">
                            <input type="email" class="form-control" placeholder="City of Departure">
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Delivery City">
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <div class="form-group form-group-select">
                            <select class="form-control">
                              <option>Incoterms</option>
                              <option>Incoterms 1</option>
                              <option>Incoterms 2</option>
                            </select>
                          </div>
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-6 col-md-4 col-lg-4 d-flex">
                          <div class="form-group mr-20">
                            <input type="email" class="form-control" placeholder="Weight">
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" placeholder="Height">
                          </div>
                        </div><!-- /.col-lg-3 -->
                        <div class="col-sm-6 col-md-4 col-lg-4  d-flex">
                          <div class="form-group mr-20">
                            <input type="email" class="form-control" placeholder="Width">
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" placeholder="Length">
                          </div>
                        </div><!-- /.col-lg-4 -->
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap">
                          <div class="form-group input-radio">
                            <label class="label-radio">Fragile
                              <input type="radio" name="radioGroup2" checked="">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Express Delivery
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Insurance
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Packaging
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <button class="btn btn__secondary btn__block">Request A Quote</button>
                        </div><!-- /.col-lg-12 -->
                      </div>
                    </div><!-- /.request__form-body -->
                    <div class="widget widget-download bg-theme">
                      <div class="widget__content">
                        <h5>Industry<br>Solutions!</h5>
                        <p>Our worldwide presence ensures the timeliness, cost efficiency and compliance adherence
                          required to ensure your production timelines are met.</p>
                        <a href="#" class="btn btn__secondary btn__hover2 btn__block">
                          <span>Download 2019 Brochure</span><i class="icon-arrow-right"></i>
                        </a>
                      </div><!-- /.widget__content -->
                    </div><!-- /.widget-download -->
                  </div><!-- /.request-quote-panel-->
                </div><!-- /.tab -->
                <div class="tab-pane fade" id="track">
                  <div class="request-quote-panel">
                    <div class="request__form-body">
                      <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="form-group">
                            <label>Shipment Type</label>
                            <div class="form-group form-group-select">
                              <select class="form-control">
                                <option>Packaging</option>
                                <option>Packaging 1</option>
                                <option>Packaging 2</option>
                              </select>
                            </div>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="form-group">
                            <label>Tracking Number</label>
                            <div class="form-group">
                              <textarea class="form-control"
                                placeholder="You can enter up to a maximum of 10 airway bill numbers for tracking."></textarea>
                            </div>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap">
                          <div class="form-group input-radio">
                            <label class="label-radio">Fragile
                              <input type="radio" name="radioGroup2" checked="">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Express Delivery
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Insurance
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                          <div class="form-group input-radio">
                            <label class="label-radio">Packaging
                              <input type="radio" name="radioGroup2">
                              <span class="radio-indicator"></span>
                            </label>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <button class="btn btn__secondary btn__block">Track & Trace</button>
                        </div><!-- /.col-lg-12 -->
                      </div>


                    </div><!-- /.request__form-body -->
                    <div class="widget widget-download bg-theme">
                      <div class="widget__content">
                        <h5>Industry<br>Solutions!</h5>
                        <p>Our worldwide presence ensures the timeliness, cost efficiency and compliance adherence
                          required to ensure your production timelines are met.</p>
                        <a href="#" class="btn btn__secondary btn__hover2 btn__block">
                          <span>Download 2019 Brochure</span><i class="icon-arrow-right"></i>
                        </a>
                      </div><!-- /.widget__content -->
                    </div><!-- /.widget-download -->
                  </div><!-- /.request-quote-panel-->
                </div><!-- /.tab -->
              </div><!-- /.tab-content -->
            </div><!-- /.request-form -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.Request Quote Tabs -->

    <!-- =====================
       Clients 1
    ======================== -->
    <section id="clients1" class="clients clients-1 border-top">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="carousel owl-carousel" data-slide="6" data-slide-md="4" data-slide-sm="2" data-autoplay="true"
              data-nav="false" data-dots="false" data-space="20" data-loop="true" data-speed="700">
              <div class="client">
                <a href="#"><img src="assets/images/clients/9.png" alt="client"></a>
              </div><!-- /.client -->
              <div class="client">
                <a href="#"><img src="assets/images/clients/10.png" alt="client"></a>
              </div><!-- /.client -->
              <div class="client">
                <a href="#"><img src="assets/images/clients/11.png" alt="client"></a>
              </div><!-- /.client -->
              <div class="client">
                <a href="#"><img src="assets/images/clients/12.png" alt="client"></a>
              </div><!-- /.client -->
              <div class="client">
                <a href="#"><img src="assets/images/clients/13.png" alt="client"></a>
              </div><!-- /.client -->
              <div class="client">
                <a href="#"><img src="assets/images/clients/11.png" alt="client"></a>
              </div><!-- /.client -->
            </div><!-- /.carousel -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.clients 1 -->

    <!-- ======================
           banner 1
      ========================= -->
    <section id="banner1" class="banner banner-1 p-0 bg-gray">
      <div class="container-fluid col-padding-0">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="inner-padding">
              <div class="heading heading-3 mb-30">
                <i class="icon-tag"></i>
                <span class="heading__subtitle">Affordable Price, Certified Forwarders</span>
                <h2 class="heading__title">Safe, Reliable & Express Logistic Solutions That Saves Your Time!</h2>
                <p class="heading__desc">We pride ourselves on providing the best transport and shipping services
                  available allover the world. Our skilled personnel, utilising the latest communications, tracking and
                  processing software, combined with decades of experience.</p>
              </div><!-- /.heading -->
              <a href="#" class="btn btn__primary">Schedule An Appointment</a>
            </div>
          </div><!-- /.col-lg-6 -->
          <div class="col-sm-12 col-md-12 col-lg-6 bg-overlay  background-banner">
            <div class="bg-img"><img src="assets/images/banners/1.jpg" alt="background"></div>
            <div class="skills skills-panel">
              <div class="heading mb-30">
                <h2 class="heading__title">What We Achieved!</h2>
                <p class="heading__desc">Fulfill our dedicated promise to deliver innovative & dynamic solutions to our
                  customers to fit their needs.</p>
              </div><!-- /.heading -->
              <!-- progress 1 -->
              <div class="progress-item">
                <h6 class="progress__title">Warehousing</h6>
                <span class="progress__percentage"></span>
                <div class="progress">
                  <div class="progress-bar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" role="progressbar">
                  </div>
                </div><!-- /.progress -->
              </div><!-- /.progress-item  -->
              <!-- progress 2 -->
              <div class="progress-item">
                <h6 class="progress__title">Air Freight</h6>
                <span class="progress__percentage"></span>
                <div class="progress">
                  <div class="progress-bar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" role="progressbar">
                  </div>
                </div><!-- /.progress -->
              </div><!-- /.progress-item  -->
              <!-- progress 3 -->
              <div class="progress-item">
                <h6 class="progress__title">Ocean Freight</h6>
                <span class="progress__percentage"></span>
                <div class="progress">
                  <div class="progress-bar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" role="progressbar">
                  </div>
                </div><!-- /.progress -->
              </div><!-- /.progress-item  -->
              <!-- progress 4 -->
              <div class="progress-item">
                <h6 class="progress__title">Road Freight</h6>
                <span class="progress__percentage"></span>
                <div class="progress">
                  <div class="progress-bar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" role="progressbar">
                  </div>
                </div><!-- /.progress -->
              </div><!-- /.progress-item  -->
              <!-- progress 8 -->
              <div class="progress-item">
                <h6 class="progress__title">Supply Chain</h6>
                <span class="progress__percentage"></span>
                <div class="progress">
                  <div class="progress-bar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" role="progressbar">
                  </div>
                </div><!-- /.progress -->
              </div><!-- /.progress-item  -->
            </div><!-- /.skills -->
          </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.banner 1 -->

    <!-- ========================
            Footer
    ========================== -->
    <footer id="footer" class="footer">
      <div class="footer-newsletter">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
              <div class="footer__newsletter-text">
                <h6>Our Newsletter</h6>
                <p>Sign up for industry news & insights.</p>
              </div><!-- /.footer-newsletter-text -->
            </div><!-- /.col-xl-3-->
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-7">
              <form class="footer__newsletter-form d-flex flex-wrap">
                <div class="form-group d-flex flex-1 mb-20 mr-1">
                  <input type="text" class="form-control" placeholder="Your Name">
                  <input type="email" class="form-control" placeholder="Your Email Address">
                </div>
                <button class="btn btn__primary btn__hover3 mb-20">Submit Now!</button>
              </form>
            </div><!-- /.col-xl-7 -->
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-2">
              <div class="social__icons justify-content-end">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
              </div><!-- /.social-icons -->
            </div><!-- /.col-xl-2 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div>
      <div class="footer-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4 footer__widget footer__widget-about">
              <div class="footer__widget-content">
                <img src="assets/images/logo/logo-small.png" alt="logo" class="footer-logo">
                <p>Our global Optimecs expertise, advanced supply chain technology & customized Optimecs solutions
                  will help you develop and implement successful supply.</p>
                <ul class="contact__list list-unstyled">
                  <li><span>Email:</span><span>Optime@7oroof.com</span></li>
                  <li><span>Phone:</span><span>+01234567890</span></li>
                </ul>
              </div>
            </div><!-- /.col-lg-4 -->
            <div class="col-6 col-sm-6 col-md-3 col-lg-2 footer__widget footer__widget-nav">
              <h6 class="footer__widget-title">Who We Are</h6>
              <div class="footer__widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Meet Our Team</a></li>
                    <li><a href="#">News & Media</a></li>
                    <li><a href="#">Case Studies</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Careers</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-lg-2 -->
            <div class="col-6 col-sm-6 col-md-3 col-lg-2 footer__widget footer__widget-nav">
              <h6 class="footer__widget-title">What We Do</h6>
              <div class="footer__widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="#">Warehousing</a></li>
                    <li><a href="#">Air Freight</a></li>
                    <li><a href="#">Ocean Freight</a></li>
                    <li><a href="#">Road Freight</a></li>
                    <li><a href="#">Supply Chain</a></li>
                    <li><a href="#">Packaging</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-lg-2 -->
            <div class="col-6 col-sm-6 col-md-3 col-lg-2 footer__widget footer__widget-nav">
              <h6 class="footer__widget-title">Who We Serve</h6>
              <div class="footer__widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="#">Retail & Consumer</a></li>
                    <li><a href="#">Sciences & Healthcare</a></li>
                    <li><a href="#">Industrial & Chemical</a></li>
                    <li><a href="#">Power Generation</a></li>
                    <li><a href="#">Food & Beverage</a></li>
                    <li><a href="#">Oil & Gas</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-lg-2 -->
            <div class="col-6 col-sm-6 col-md-3 col-lg-2 footer__widget footer__widget-nav">
              <h6 class="footer__widget-title">Quick Links</h6>
              <div class="footer__widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="#">Request A Quote</a></li>
                    <li><a href="#">Track & Trace</a></li>
                    <li><a href="#">Find A Location</a></li>
                    <li><a href="#">Global Agents</a></li>
                    <li><a href="#">Help & FAQ</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-lg-2 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-top -->
      <div class="footer-bottom">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-sm-12 col-md-6 col-lg-6">
              <div class="footer__copyright">
                <span>&copy; 2019 Optime, with Love by</span>
                <a href="http://themeforest.net/user/7oroof">7oroof.com</a>
              </div><!-- /.Footer-copyright -->
            </div><!-- /.col-lg-6 -->
            <div class="col-sm-12 col-md-6 col-lg-6">
              <nav>
                <ul class="list-unstyled footer__copyright-links d-flex flex-wrap justify-content-end">
                  <li><a href="#">Terms & Conditions </a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Sitemap</a></li>
                </ul>
              </nav>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.Footer-bottom -->
    </footer><!-- /.Footer -->

    <div class="module__search-container">
      <i class="fa fa-times close-search"></i>
      <form class="module__search-form">
        <input type="text" class="search__input" placeholder="Type Words Then Enter">
        <button class="module__search-btn"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- /.module-search-container -->

    <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>