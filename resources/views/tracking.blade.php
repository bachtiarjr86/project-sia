

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="Optime Logistics &amp; Transportation Templatei">
  <link href="assets/images/favicon/favicon.png" rel="icon">
  <title>Proses Tracking - JNJ Express</title>
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:400,500,700%7cPoppins:400,600,700&display=swap">
  <link rel="stylesheet" href="{{asset('assets/css/libraries.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
  <div class="wrapper">
    <!-- =========================
        Header
    =========================== -->    
    <header id="header" class="header header-transparent" style="background-color: #f2a51a;">
      <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html">
            <img src="{{asset('assets/images/logo/logo-light.png')}}" class="logo-light" alt="logo">
            <img src="{{asset('assets/images/logo/logo-dark.png')}}" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>         
          <div class="collapse navbar-collapse" id="mainNavigation">         
            <ul class="navbar-nav ml-auto">                       
              <li class="nav__item with-dropdown">
                <a href="/" class="dropdown-toggle nav__item-link">Home</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="#" class="dropdown-toggle nav__item-link active">Proses Tracking</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="services.html" class="dropdown-toggle nav__item-link">Status Pengiriman</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
              <li class="nav__item with-dropdown">
                <a href="/cekongkir" class="dropdown-toggle nav__item-link">Cek Ongkir</a>
                <i class="fa fa-angle-right" data-toggle="dropdown"></i>
              </li><!-- /.nav-item -->
                <li class="nav__item with-dropdown">
                    <a href="#" class="dropdown-toggle nav__item-link" style="color: #10375c;" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav__item"> 
                            <a href="{{ route('logout') }}" class="nav__item-link"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li><!-- /.nav-item -->
                  <!-- /.nav-item -->
                </ul><!-- /.dropdown-menu -->                
              </li><!-- /.nav-item -->                                      
            </ul><!-- /.navbar-nav -->
          </div><!-- /.navbar-collapse -->          
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <br>
    <!-- ========================
        Request Quote Tabs
    =========================== -->
    <section id="requestQuoteTabs" class="request-quote request-quote-tabs p-0">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="request__form">
              <nav class="nav nav-tabs">
                <a class="nav__link" data-toggle="tab" href="#track">Proses Tracking</a>
              </nav>
              <div class="tab-content">
                
                <div class="tab-pane fade" id="track">
                  <div class="request-quote-panel">
                    <div class="request__form-body">
                      <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="form-group">
                            <label>Jenis Pengiriman</label>
                            <div class="form-group form-group-select">
                              <select class="form-control">
                                <option>JNE - YES</option>
                                <option>JNE - REG</option>
                                <option>TIKI</option>
                                <option>Pos Indonesia</option>
                              </select>
                            </div>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="form-group">
                            <label>Nomor Resi</label>
                            <div class="form-group">
                              <textarea class="form-control"
                                placeholder="Masukkan no. resi sesuai jenis pengiriman" id="resi" name="resi">
                              </textarea>
                            </div>
                          </div>
                        </div><!-- /.col-lg-12 -->
                        
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <a href="{{ url('kirim') }}" class="btn btn__secondary btn__block">Lacak Paket</a>
                        </div><!-- /.col-lg-12 -->
                      </div>
                    </div><!-- /.request__form-body -->                    
                  </div><!-- /.request-quote-panel-->
                </div><!-- /.tab -->
              </div><!-- /.tab-content -->
            </div><!-- /.request-form -->
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.Request Quote Tabs -->        

    <div class="module__search-container">
      <i class="fa fa-times close-search"></i>
      <form class="module__search-form">
        <input type="text" class="search__input" placeholder="Type Words Then Enter">
        <button class="module__search-btn"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- /.module-search-container -->

    <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
  </div><!-- /.wrapper -->

  <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>
