<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLacaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lacaks', function (Blueprint $table) {
            $table->increments('id_lacak');
            $table->unsignedInteger('no_resi');
            $table->foreign('no_resi')->references('no_resi')->on('kirims');
            $table->date('tgl_lacak');
            $table->string('username')->nullabel();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lacaks');
    }
}
