<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKirimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kirims', function (Blueprint $table) {
            $table->increments('no_resi');
            $table->date('tgl_kirim');
            $table->string('kode_kota_pengirim'); 
            $table->string('nama_pengirim');
            $table->integer('telp_pengirim');
            $table->string('kode_kota_penerima');
            $table->string('nama_tujuan');
            $table->integer('telp_tujuan');
            $table->string('alamat_tujuan');
            $table->string('nama_barang');
            $table->string('tipe_paket');
            $table->integer('berat');
            $table->integer('biaya');
            $table->string('nama_penerima');
            $table->date('waktu_penerima');
            $table->string('keterangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kirims');
    }
}
